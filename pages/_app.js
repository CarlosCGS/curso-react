import '../sass/main.scss'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ConfigProvider } from 'antd'
import frEs from 'antd/lib/locale/es_ES'
function MyApp ({ Component, pageProps }) {
  const queryClient = new QueryClient()

  return (
    <QueryClientProvider client={queryClient}>
      <ConfigProvider locale={frEs}>
        <Component {...pageProps} />
      </ConfigProvider>
    </QueryClientProvider>
  )
}

export default MyApp
