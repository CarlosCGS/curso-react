import Table from 'components/Table'
import { useState } from 'react'
import { Switch } from 'antd'

function Index () {
  const [headers, setHeaders] = useState([{ name: 'nombre', isVisble: true }, { name: 'apellido', isVisble: true }, { name: 'ciudad', isVisble: true }])
  const records = [{ nombre: 'carlos', apellido: 'garcia', ciudad: 'cdmx' }, { nombre: 'david', apellido: 'marquez', ciudad: 'CDMX' }]

  const renerSwitchHeaders = headers.map(({ name, isVisble }) => {
    return (
      <div key={name}>
        <span className='is-capitalized'>{name}</span>
        <Switch
          onChange={(checked) => {
            setHeaders(headers.map(header => {
              if (header.name === name) {
                header.isVisble = checked
              }
              return header
            }))
          }}
          checked={isVisble}
          className='has-margin-5'
          size='small'
          checkedChildren='Mostrar'
          unCheckedChildren='No Mostrar'
        />
      </div>
    )
  })

  return (
    <div className='main has-padding-50'>
      <div className='has-padding-10'>
        {renerSwitchHeaders}

      </div>
      <Table records={records} headers={headers} />
    </div>
  )
}

export default Index
