import React, { useState } from 'react'
import { Input } from 'antd'
import { UserOutlined } from '@ant-design/icons'

const InputItem = ({ value }) => {
  const [text, setText] = useState(value)
  const [isBordered, setIsBordered] = useState(false)
  return <Input prefix={<UserOutlined />} placeholder='Escribe...' onFocus={() => { setIsBordered(true) }} onBlur={() => { setIsBordered(false) }} bordered={isBordered} onChange={(e) => { setText(e.target.value) }} value={text} />
}

const ListItem = ({ children, style }) => {
  return <li style={style}>{children}</li>
}

const List = ({ items, renderItem }) => {
  return (
    <ul>
      {items.map((item, index) => (
        <ListItem key={index} style={item.style}>
          {renderItem ? renderItem(item) : item.text}
        </ListItem>
      ))}
    </ul>
  )
}

const Index = () => {
  const items = [
    { text: 'Item 1', style: { color: 'red' } },
    { text: 'Item 2', style: { color: 'blue' } },
    { text: 'Item 3', style: { color: 'green' } }
  ]

  const customRender = (item) => {
    //  return  <span style={{ fontWeight: 'bolder', color:item.text==='Item 1'&&'yellow'}}>{item.text}</span>
    return <InputItem value={item.text} />
  }

  const customRenderSimple = (item) => {
    return <span style={{ fontWeight: 'bolder' }}><UserOutlined />{item.text}</span>
    // return <InputItem value={item.text} />
  }

  return (
    <div className='main'>

      <div className='is-flex'>
        <div className='has-margin-30'>
          <h1>List Example</h1>
          <List items={items} renderItem={customRenderSimple} />
        </div>

        <div className='has-margin-30'>
          <h2>List with Custom Render</h2>
          <List items={items} renderItem={customRender} />
        </div>

      </div>
    </div>
  )
}

export default Index
