import React from 'react'

const RecordsTable = ({ records = [] }) => {
  const renderRecords = records.map((record, index) => {
    return (
      <tr key={index}>
        {Object.values(record).map(value => <td key={value}>{value}</td>)}
      </tr>
    )
  })

  return renderRecords
}

const Table = ({ records = [], headers = [] }) => {
  const renderHeaders = headers.map(({ name }) => {
    return (
      <th key={name} className='px-2'>
        {name}
      </th>
    )
  })

  return (
    <div>
      <table>
        <thead>
          {renderHeaders}
        </thead>
        <tbody>
          <RecordsTable records={records} />
        </tbody>
      </table>
    </div>
  )
}

export default Table
